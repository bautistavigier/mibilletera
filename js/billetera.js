/**
 * Objeto de Billetera.
 * @type {{usuario: string, total: number, movimientos: Array, sumar: Billetera.sumar, restar: Billetera.restar, cargar: Billetera.cargar, guardar: Billetera.guardar, resetear: Billetera.resetear}}
 */
var Billetera = {
	usuario: '',
	total: 0,
	movimientos: [],
	sumar: function (monto, motivo) {
		monto = parseFloat(monto);
		this.total += monto;
		var ingreso = {
			'monto': monto,
			'motivo': motivo,
			'fecha': now(),
			'tipo': 'ingreso'
		};
		this.movimientos.push(ingreso);
		this.guardar();
	},
	restar: function (monto, motivo) {
		monto = parseFloat(monto);
		this.total -= monto;
		var egreso = {
			'monto': monto,
			'motivo': motivo,
			'fecha': now(),
			'tipo': 'egreso'
		};
		this.movimientos.push(egreso);
		this.guardar();

	},
	cargar: function () {
		var data = JSON.parse(localStorage.getItem('storedData'));
		this.usuario = data.usuario;
		this.total = data.total;
		this.movimientos = data.movimientos;
	},
	guardar: function () {
		var data = {
			'usuario': this.usuario,
			'total': this.total,
			'movimientos': this.movimientos
		};
		localStorage.setItem('storedData', JSON.stringify(data));
	},
	resetear: function () {
		this.usuario = '';
		this.total = 0;
		this.movimientos = [];
		localStorage.removeItem('storedData');
	}
};