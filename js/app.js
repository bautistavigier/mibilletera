/**
 * Valida el ingreso de datos del usuario
 * @param monto
 * @param motivo
 * @returns {boolean}
 */
function movimientoValido(monto, motivo) {
	var valido = true;
	if (!monto || !motivo ) return false;

	if (isNaN(monto) || !isNaN(motivo)) {
		valido = false;
	}

	return valido;
}

/**
 * Actualiza el resumen de movimientos.
 */
function actualizarResumen() {
	var lista = $('#listaResumen');
	lista.empty();
	for (var i in Billetera.movimientos) {
		var movi = Billetera.movimientos[i];
		var spanMovimiento = $('<span></span>');
		var signo = movi.tipo == 'ingreso' ? '+' : '-';
		var color = movi.tipo == 'ingreso' ? '#128f00' : '#a10000';

		spanMovimiento.html( signo + ' $' + movi.monto);
		spanMovimiento.addClass('span-mov');
		spanMovimiento.css({
			display: 'inline-block',
			position: 'absolute',
			right: '10px',
			fontSize: '18px',
			color: color
		});

		var liResumen = $('<li class="ui-li-static ui-body-inherit"></li>');
		liResumen.html('<span class="fecha-mov">' + movi.fecha + '</span>  ' + movi.motivo);
		lista.append(liResumen);
		liResumen.append(spanMovimiento);
	}
	var totalColor = Billetera.total > 0 ? 'green' : 'red';
	var total = $('<li class="resumen-total ' + totalColor + ' ui-li-static ui-body-inherit ui-last-child"></li>');
	total.html("Total: <span>$ " + Billetera.total + "</span>");
	lista.append(total);
}


/**
 * Actualiza los valores del Home
 */
function actualizarValores() {
	$('#dineroDisponible').html(Billetera.total);
	$('#nombreElejido').html(Billetera.usuario);
	var sim = $('#sim');
	if(Billetera.total > 0){
		sim.css('color','#128f00');
	}else{
		sim.css('color','#a10000');
	}
}


/************* ONLOAD *************************************************************************************************/
$(function ($) {
	$(window).on('load', function () {
		if (localStorage.getItem('storedData') != null) {
			Billetera.cargar();
			actualizarValores();
			window.location = '#home';
		}
	});
});


/************* COMENZAR *********************/
$('#btnComenzar').click(function(e){
	if ($('.error')) $('.error').remove();

	var nombre = $('#ingresarNombre');
	var dineroIngresado = $('#ingresarDinero');

	if ( !isNaN(dineroIngresado.val()) && dineroIngresado.val() && nombre.val() && isNaN(nombre.val()) ) {
		Billetera.resetear();
		Billetera.usuario = nombre.val();
		Billetera.sumar(parseFloat(dineroIngresado.val()), 'Dinero Inicial');
		actualizarValores();
		window.location = '#home';
	} else {
		e.preventDefault();
		var error = $('<p></p>');
		error.html('Ingrese su nombre y un número válido');
		error.addClass('error');
		$('#datosForm').append(error);
	}

});

/************* EVENTS *************************************************************************************************/

$('#irResumen').click(function () {
	actualizarResumen();
});

$.each($('.irResumen'), function () {
	$(this).on('click', function () {
		actualizarResumen();
	});
});

$('#datosForm').on("keydown",function(e){
	if (e.keyCode == 13) {
		$('#btnComenzar').trigger('click');
		console.log(Billetera);
	}
});

$('#restarForm').on("keydown",function(e){
	if (e.keyCode == 13) {
		$('#gastoListo').trigger('click');
	}
});

$('#sumarForm').on("keydown",function(e){
	if (e.keyCode == 13) {
		$('#ingresoListo').trigger('click');
	}
});


/************* INGRESO ************************************************************************************************/
$('#ingresoListo').click(function(e){
	if ($('.error')) $('.error').remove();

	var concepto = $('#nuevaCategoriaIngreso').val();
	var ingreso = $('#nuevoIngreso').val();

	if (movimientoValido(ingreso, concepto)) {

		Billetera.sumar(ingreso, concepto);
		actualizarResumen();
		actualizarValores();
		window.location = '#home';

	} else {
		e.preventDefault();
		var error = $('<p></p>');
		error.html('Ingrese los datos correctamente');
		error.addClass('error');
		$('#agregarIngresos').find('main').append(error);
	}

});


/************* EGRESO *************************************************************************************************/
$('#gastoListo').click(function(e){
	if ($('.error')) $('.error').remove();

	var concepto = $('#nuevaCategoriaGasto').val();
	var egreso = $('#nuevoGasto').val();

	if (movimientoValido(egreso, concepto)) {

		Billetera.restar(egreso, concepto);
		actualizarResumen();
		actualizarValores();
		window.location = '#home';

	} else {
		e.preventDefault();
		var error = $('<p></p>');
		error.html('Ingrese los datos correctamente');
		error.addClass('error');
		$('#agregarGasto').find('main').append(error);
	}


});


/************* CERRAR APLICACIÓN **************************************************************************************/
$.each($('.cerrar-btn'), function () {
	$(this).click(function () {
		navigator.app.exitApp();
	});
});


/************* CERRAR SESIÓN ******************************************************************************************/
$.each($('.cerrar-sesion'), function () {
	$(this).click(function () {
		Billetera.resetear();
		$('#dineroDisponible').html('');
		$('#nombreElejido').html('');
		$('#ingresarNombre').val('');
		$('#ingresarDinero').val('');
	});
});


